const ADD_TYPE = 'ADD_TYPE';

const reducer = (state, action) => {
  console.log(action);
  switch (action.type) {
    case ADD_TYPE:
      return {
        num: state.num + 1,
      };
    default:
      return state;
  }
};

const store = createStore(reducer, { num: 0 });
const store1 = createStore(reducer, { num: 0 });

store.subscribe(() => {
  console.log('subscribe', store.getState());
});

store.subscribe(() => {
  console.log('subscribe1', store.getState());
});

console.log('init', store.getState());

store.dispatch({
  type: ADD_TYPE,
});

console.log('add--dispatch', store.getState());

store.dispatch({
  type: ADD_TYPE,
});

console.log('add--dispatch', store.getState());

console.log(store1.getState());

const _dispatch = store.dispatch;

store.dispatch = (action) => {
  if (typeof action === 'function') {
    action(_dispatch);
  } else {
    _dispatch(action);
  }
};

store.dispatch((dispatch) => {
  setTimeout(() => {
    dispatch({
      type: ADD_TYPE,
    });
  }, 200);
});

// 方法调用
var oname = 'window_name';

const o = {
  oname: 'o_name',
  run(age) {
    console.log(this.oname, age);
    // this o
    this._run = () => {
      console.log('箭头函数', this.oname);
    };
  },
  test: () => {
    console.log('test', this.oname);
  },
};

const o1 = {
  oname: 'o1_name',
};

o.run(); // o_name  方法调用 指向拥有该方法的对象 o拥有run方法 this指向o

const _run = o.run; // 新的匿名函数

_run(); // window_name 调用全局匿名函数 普通调用 this指向window

o.run.call(null, 10); // window_name 修改this指向 null指向全局对象

o.run.apply(o1, [10]); // o1_name  修改this指向

const _run1 = o.run.bind(null, 10);

_run1(); // window_name

_run1.call(o1, 10); // window_name

o.test(); // window_name;  箭头函数的this指向在定义时形成 不管调用方式是什么都不会改变

o.test.call(o1); // window_name;

o._run(); // o_name

window._run(); // window_name

o1._run(); // o1_name

// call和apply的区别 第二个参数不同，apply第二个参数是实参集合 call可以多个参数 修改this指向之后立即执行

// bind 不会执行返回修改this指向之后的函数 而且优先级最高

// this 环境上下文对象 会根据调用方式不同发生变化（箭头函数不会）
