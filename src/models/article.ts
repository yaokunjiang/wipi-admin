import { Effect, Reducer } from 'umi';
import { getArticleList } from '@/services/user';
export interface ActicleModelState {
  list: any[];
  sum: number;
}

export interface ActicleModel {
  state: ActicleModelState;
  effects: {
    getList: Effect;
  };
  reducers: {
    setList: Reducer;
  };
}

const models: ActicleModel = {
  state: {
    list: [],
    sum: 0,
  },
  effects: {
    *getList(action, { call, put }) {
      const { data } = yield call(getArticleList, { page: 1, pageSize: 10 });
      yield put({
        type: 'setList',
        payload: data,
      });
    },
  },
  reducers: {
    setList(option, { payload }) {
      return {
        list: payload[0],
        sum: payload[1],
      };
    },
  },
};

export default models;
// state
// reducers 同步数据
// effect  异步数据 *
// subscribes
