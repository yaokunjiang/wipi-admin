import { RequestConfig, getDvaApp } from 'umi';
import { createLogger } from 'redux-logger';
import { message } from 'antd';

export const layout = () => {
  return {
    rightContentRender: () => <div>rightContentRender</div>,
    footerRender: () => <div>footer</div>,
    onPageChange: () => {
      console.log('onPageChange');
    },
    menuHeaderRender: undefined,
  };
};

export interface InitialState {
  role: string;
  [propName: string]: any;
}

export const getInitialState = async () => {
  try {
    const userInfo = JSON.parse(localStorage.getItem('userInfo') as string);
    return (
      userInfo || {
        role: '',
      }
    );
  } catch (error) {
    return {
      role: '',
    };
  }
};

export const dva = {
  config: {
    onAction: createLogger(),
    onError(e: Error) {
      message.error(e.message, 3);
    },
  },
};

// window.Dva = getDvaApp();
//dvajs  简化redux操作流程 继承redux-saga
